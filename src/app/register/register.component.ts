import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../data/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Input() id: number;
  myForm: FormGroup;
  agreeToTerms = false;
  termsErrMsg = '';
  date: Date = new Date();



  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder,private toastr: ToastrService
    ,private spinner: NgxSpinnerService,private userService:UserService) 
     { }

  ngOnInit() {
    this.createForm();
  }
  closeModal() {
    this.activeModal.close('Modal Closed');
  }//referralCode
  private createForm() {
    this.myForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      phonenumber: ['', [Validators.required,Validators.pattern(/^(?=.*[0-9])[0-9]+$/),Validators.maxLength(10)
        , Validators.minLength(10)]],
      email: ['', [Validators.required,
      Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      IdNumber: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(13)
      , Validators.minLength(13)]],
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
      confirmPassword: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
      termsAgreement: [false]
    });
  }

  submitForm() {
    console.log(this.myForm.value);
    this.spinner.show();

    if (this.myForm.value.password !== this.myForm.value.confirmPassword) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      return this.toastr.error('Passwords Are Not The Same!', 'Error');
    }
   
    if(this.myForm.value.phonenumber.toString().substring(0,2) === '01' || this.myForm.value.phonenumber.toString().substring(0,2) === '02' || this.myForm.value.phonenumber.toString().substring(0,2) === '03' || this.myForm.value.phonenumber.toString().substring(0,2) === '04' || this.myForm.value.phonenumber.toString().substring(0,2) === '05') {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      return this.toastr.error('Please enter a valid mobile number');
    }
    if(this.myForm.value.IdNumber.toString().substring(0,2) === this.date.getFullYear().toString().substring(2,4)){
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      return this.toastr.error('Please enter a valid SA Id number');

    }


    if (!this.agreeToTerms) {
      // return this.toastr.error('Please agree to the Terms & Conditions');
      this.spinner.hide();
      return this.termsErrMsg = 'Please agree to the terms';
    }
    
    if (!this.myForm.valid) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
    console.log(this.myForm.value['firstName']);

      return this.toastr.error('Please Fill In The Fields...', 'Error');
    }
    this.userService.saveUser(this.myForm.value).subscribe((res:any) => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        location.reload();
    }, 5000);
    localStorage.setItem('firstNameAfterReg',this.myForm.value['firstName'])

      this.toastr.success('You registered successfully!');
      localStorage.setItem('ID', res);
      this.myForm.reset();
      this.closeModal();
    }, error => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
    if(error.error.text != '')
    {
      this.toastr.error(error.error.text, 'Registration Error');

    }
    
    else{
      this.toastr.error('Oops! Something went wrong on the server', 'Registration Error');
      
    }
      console.log(error);
    });

  }
  

toggle() {
  this.agreeToTerms = true;
  console.log(this.agreeToTerms);
}
  

}
