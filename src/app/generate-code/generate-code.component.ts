import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../data/user.service';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';

@Component({
  selector: 'ngx-generate-code',
  templateUrl: './generate-code.component.html',
  styleUrls: ['./generate-code.component.scss']
})
export class GenerateCodeComponent implements OnInit {
  @Input() id: number;
  myForm: FormGroup;
  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder,
    private modalService: NgbModal,private toastr: ToastrService
    ,private svc: UserService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.createForm();
  }
  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      email: ['', [Validators.required,
        Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]]

    });
  }
 
  submitForm() {
    this.spinner.show();

    if (!this.myForm.valid) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);

      return this.toastr.error('Please Fill In The Fields...', 'Error');
    }
    this.svc.generatePasswordCode(this.myForm.value.email).subscribe(res => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      this.toastr.success('Congratulations! A unique code has been sent to your email.');

      this.myForm.reset();
      this.closeModal();
    }, error => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      this.toastr.error('Oops! Something went wrong on the server', 'Error');
      console.log(error);
    });

  }
  openFormModal() {
    const modalRef = this.modalService.open(ResetPasswordComponent,
      {size: 'lg',
      centered: true });
    modalRef.componentInstance.id = 2;
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    // this.toggleNavbar();
  }
}
