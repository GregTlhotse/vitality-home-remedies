import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../data/user.service';
import { DoctorService } from '../pages/data/doctor.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.scss']
})
export class DoctorComponent implements OnInit {

  myForm: FormGroup;
  emails: FormArray;

  constructor(private formBuilder: FormBuilder,
    private spinner:NgxSpinnerService,
    private toster:ToastrService,private doctorService:DoctorService,private userService:UserService,private router: Router) { }

  ngOnInit() {
    this.createForm();
    this.getUser();
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      doctorEmail: ['', []],
      FullName: ['', [Validators.required,]],
      email: ['', [Validators.required,
        Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
        mobileNumber: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern(/^(?=.*[0-9])[0-9]+$/)]],
      message: ['', [Validators.required]]
      
    });
  }
  submitForm(){
    console.log(this.myForm.value)
    this.myForm.value.doctorEmail = sessionStorage.getItem('doctorEmail');
    this.spinner.show();
    this.doctorService.requestDoctor(this.myForm.value).subscribe(data =>{
      console.log(data);
      this.toster.success("Email sent successfully.");
        this.spinner.hide();
        this.router.navigateByUrl('/home');

      
    }
    ,error => {
      console.log(error);
      console.log(error.error["text"]);
      if(error.error["text"] === "Email sent"){
        this.toster.success("Email sent successfully.");
        this.spinner.hide();
        this.router.navigateByUrl('/home');


      }
      else{
        this.toster.info("Oops! Our system don't have relevant Doctors to help based on your message.");
      }
        this.spinner.hide();
    })
  }
  getUser(){
    this.spinner.show();
    this.userService.getCurrentUser(localStorage.getItem('ID')).subscribe(x =>{
      this.loadDataFromDatabase(x);
      this.spinner.hide();
    })
  }
  loadDataFromDatabase(user) {
    console.log(user);
    this.myForm.patchValue({
                              FullName: user.firstName,
                              lastName:user.lastName,
                              mobileNumber:user.phonenumber,
                              email: user.email,
                             
                              
    });
  }
  
}
