import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../data/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @Input() id: number;
  myForm: FormGroup;
  closeResult: string;


  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder,private toastr: ToastrService
    ,private userService:UserService,private spinner: NgxSpinnerService,private modlService: NgbModal,private router: Router) { }

  ngOnInit() {
    this.createForm();
     this.getUser();
  }
  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      id: ['', []],
      firstName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      phonenumber: ['', [Validators.required,]],
      email: ['', [Validators.required,
      Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      IdNumber: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(13)
      , Validators.minLength(13)]],
  
    });
  }

  submitForm() {
    this.spinner.show();
    console.log(this.myForm.value);

  
    if (!this.myForm.valid) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);

      return this.toastr.error('Please Fill In The Fields...', 'Error');
    }
    this.userService.saveUser(this.myForm.value).subscribe(res => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        location.reload();
    }, 5000);
    localStorage.setItem('firstNameAfterReg',this.myForm.value['firstName'])
    localStorage.setItem('name', this.myForm.value['firstName']);

      this.toastr.success('Your details have been updated');
      this.myForm.reset();
      this.closeModal();
    }, error => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      this.toastr.error('Oops! Something went wrong on the server', 'Registration Error');
      console.log(error);
    });

  }
  
  loadDataFromDatabase(user) {
    console.log(user);
    this.myForm.patchValue({
                              id: user.id,
                              firstName: user.firstName,
                              lastName:user.lastName,
                              phonenumber:user.phonenumber,
                              IdNumber:user.idNumber,
                              email: user.email,
                             
                              
    });
  }
  open(content) {
    this.modlService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }


  getUser(){
    this.spinner.show();
    this.userService.getCurrentUser(localStorage.getItem('ID')).subscribe(x =>{
      this.loadDataFromDatabase(x);
      this.spinner.hide();
    })
  }
  delete(){
    this.userService.deleteUser(this.myForm.value.email).subscribe(x => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        location.reload();
    }, 5000);
      this.toastr.success('Your account has been deleted!');
      localStorage.removeItem('name');
  sessionStorage.removeItem('token');
  localStorage.removeItem('ID');
  this.router.navigate(['/home']);
    },error =>{
      this.toastr.error('Failed while deleting');
    })
  }
}
