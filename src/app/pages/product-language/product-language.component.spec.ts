import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductLanguageComponent } from './product-language.component';

describe('ProductLanguageComponent', () => {
  let component: ProductLanguageComponent;
  let fixture: ComponentFixture<ProductLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
