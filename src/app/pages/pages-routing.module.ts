import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';
import { AddLanguageComponent } from './add-language/add-language.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AddUsersComponent } from './add-users/add-users.component';
import { ProductLanguageComponent } from './product-language/product-language.component';
import { ProductDoctorComponent } from './product-doctor/product-doctor.component';
import { DoctorReportComponent } from './reports/doctor-report/doctor-report.component';
import { ProductReportComponent } from './reports/product-report/product-report.component';
import { LanguageReportComponent } from './reports/language-report/language-report.component';
import { UserReportComponent } from './reports/user-report/user-report.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'iot-dashboard',
      component: DashboardComponent,
    }
    ,
    {
      path: 'add-doctor',
      component: AddDoctorComponent,
    }
    ,
    {
      path: 'add-lang',
      component: AddLanguageComponent,
    }
    ,
    {
      path: 'add-prod',
      component: AddProductComponent,
    }
    ,
    {
      path: 'add-prod-language',
      component: ProductLanguageComponent,
    }
    ,
    {
      path: 'add-prod-doctor',
      component: ProductDoctorComponent,
    }
    ,
    {
      path: 'add-users',
      component: AddUsersComponent,
    }
    ,
    {
      path: 'doctor-report',
      component: DoctorReportComponent,
    }
    ,
    {
      path: 'product-report',
      component: ProductReportComponent,
    }
    ,
    {
      path: 'language-report',
      component: LanguageReportComponent,
    }
    ,
    {
      path: 'user-report',
      component: UserReportComponent,
    },
    {
      path: 'layout',
      loadChildren: () => import('./layout/layout.module')
        .then(m => m.LayoutModule),
    },
    {
      path: 'forms',
      loadChildren: () => import('./forms/forms.module')
        .then(m => m.FormsModule),
    },
    {
      path: 'ui-features',
      loadChildren: () => import('./ui-features/ui-features.module')
        .then(m => m.UiFeaturesModule),
    },
    {
      path: 'modal-overlays',
      loadChildren: () => import('./modal-overlays/modal-overlays.module')
        .then(m => m.ModalOverlaysModule),
    },
    {
      path: 'extra-components',
      loadChildren: () => import('./extra-components/extra-components.module')
        .then(m => m.ExtraComponentsModule),
    },
    {
      path: 'maps',
      loadChildren: () => import('./maps/maps.module')
        .then(m => m.MapsModule),
    },
    {
      path: 'charts',
      loadChildren: () => import('./charts/charts.module')
        .then(m => m.ChartsModule),
    },
    {
      path: 'tables',
      loadChildren: () => import('./tables/tables.module')
        .then(m => m.TablesModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
