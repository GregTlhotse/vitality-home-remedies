import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DoctorService } from '../data/doctor.service';
import { NbSpinnerComponent } from '@nebular/theme';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'ngx-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.scss'],
  providers:[NgbActiveModal]
})
export class AddDoctorComponent implements OnInit {
  closeResult:any;
  myForm: FormGroup;
docs : any[];
isEdit = false;
  constructor(private modlService: NgbModal,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private doctorServc:DoctorService,private spinner:NgxSpinnerService,public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.createForm();
    this.load();
  }
  open(content1) {
    this.createForm();
    this.isEdit = false;
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openEdit(id,content1) {
    this.getUser(id);
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      id: [null, []],
      name: ['', [Validators.required,Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2) ]],
      lastName: ['', [Validators.required,Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      Specialisation: ['', [Validators.required]],
      email: ['', [Validators.required,Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      phonenumber: ['', [Validators.required,Validators.pattern(/^(?=.*[0-9])[0-9]+$/),Validators.maxLength(10)
      , Validators.minLength(10)]],

    });
  }
  getUser(id){
    this.spinner.show();
    this.doctorServc.getCurrentUser(id).subscribe(x =>{
      this.loadDataFromDatabase(x);
      this.spinner.hide();
      this.isEdit = true;
    })
  }
  loadDataFromDatabase(provider) {
    console.log(provider);
    this.myForm.patchValue({
                              id: provider.id,
                              name: provider.name,
                              lastName: provider.lastName,
                              Specialisation:provider.specialisation,
                              email: provider.email,
                              phonenumber: provider.phonenumber,
  
    });
  }
  submitForm(){
    console.log(this.myForm.value);
    if(!this.myForm.valid){
      this.spinner.hide();
     return this.toastr.warning('Please fill in correct characters!')
    }
    this.spinner.show();
    this.doctorServc.saveDoctor(this.myForm.value).subscribe(x =>{
      console.log(x);
      this.load();
      this.spinner.hide();
      this.toastr.success('Saved Doctor Details!')
      this.modlService.dismissAll();
    })
  }
  load(){
    this.spinner.show();
    this.doctorServc.getCurrentAllDocs().subscribe(data =>{
        this.docs = data;
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }
  onDelete(email){
    this.spinner.show();
    this.doctorServc.deleteUser(email).subscribe(x => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        this.modlService.dismissAll();
        this.load();
    }, 5000);
    this.toastr.success('Doctor deleted!');

    },error =>{
      this.toastr.error('Failed while deleting');
    })
  }
}
