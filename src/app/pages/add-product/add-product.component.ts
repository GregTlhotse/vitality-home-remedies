import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DoctorService } from '../data/doctor.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService } from '../data/language.service';
import { ProductService } from '../data/product.service';

@Component({
  selector: 'ngx-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
  providers:[NgbActiveModal]

})
export class AddProductComponent implements OnInit {
  closeResult:any;
  myForm: FormGroup;
products : any[];
isEdit = false;

constructor(private modlService: NgbModal,
  private formBuilder: FormBuilder,
  private toastr: ToastrService,
  private doctorServc:DoctorService,
  private spinner:NgxSpinnerService,
  public activeModal: NgbActiveModal,
  private productService:ProductService) { }

  ngOnInit() {
    this.createForm();
    this.load();
  }
  open(content1) {
    this.createForm();
    this.isEdit = false;
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title',size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openEdit(id,content1) {
    this.getUser(id);
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      id: [null, []],
      name: ['', [Validators.required,
      , Validators.minLength(2) ]],
      description: ['', [Validators.required,
      , Validators.minLength(2)]],

    });
  }
  getUser(id){
    this.spinner.show();
    this.productService.getProduct(id).subscribe(x =>{
      this.loadDataFromDatabase(x);
      this.spinner.hide();
      this.isEdit = true;

    })
  }
  loadDataFromDatabase(provider) {
    console.log(provider);
    this.myForm.patchValue({
                              id: provider.id,
                              name: provider.name,
                              description: provider.description,

                              
  
    });
  }
  submitForm(){
    console.log(this.myForm.value);
    if(!this.myForm.valid){
      this.spinner.hide();
     return this.toastr.warning('Please fill in correct characters!')
    }
    this.spinner.show();
    this.productService.saveProduct(this.myForm.value).subscribe(x =>{
      console.log(x);
      this.load();
      this.spinner.hide();
      this.toastr.success('Saved Product Details!')
      this.modlService.dismissAll();
    })
  }
  load(){
    this.spinner.show();
    this.productService.getCurrentAllProducts().subscribe(data =>{
        this.products = data;
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }
  onDelete(id){
    this.spinner.show();
    this.productService.deleteProduct(id).subscribe(x => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        this.modlService.dismissAll();
        this.load();
    }, 5000);
    this.toastr.success('Product deleted!');

    },error =>{
      this.toastr.error('Failed while deleting');
    })
  }

}
