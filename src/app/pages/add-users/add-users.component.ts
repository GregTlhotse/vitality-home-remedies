import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DoctorService } from '../data/doctor.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService } from '../data/language.service';
import { ProductService } from '../data/product.service';
import { UserService } from '../../data/user.service';
@Component({
  selector: 'ngx-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss'],
  providers:[NgbActiveModal]

})
export class AddUsersComponent implements OnInit {

  closeResult:any;
  myForm: FormGroup;
products : any[];
isEdit = false;

constructor(private modlService: NgbModal,
  private formBuilder: FormBuilder,
  private toastr: ToastrService,
  private doctorServc:DoctorService,
  private spinner:NgxSpinnerService,
  public activeModal: NgbActiveModal,
  private productService:ProductService,private userService:UserService) { }

  ngOnInit() {
    this.createForm();
    this.load();
  }
  open(content1) {
    this.createForm();
    this.isEdit = false;
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title',size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openEdit(id,content1) {
    this.getUser(id);
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title',size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      id: ['', []],
      firstName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      phonenumber: ['', [Validators.required,Validators.pattern(/^(?=.*[0-9])[0-9]+$/),Validators.maxLength(10)
        , Validators.minLength(10)]],
      email: ['', [Validators.required,
      Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      IdNumber: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(13)
      , Validators.minLength(13)]],
      isActive:[]
    });
  }
  getUser(id){
    this.spinner.show();
    this.userService.getCurrentUser(id).subscribe(x =>{
      this.loadDataFromDatabase(x);
      this.spinner.hide();
    })
  }
  loadDataFromDatabase(user) {
    console.log(user);
    this.myForm.patchValue({
                              id: user.id,
                              firstName: user.firstName,
                              lastName:user.lastName,
                              phonenumber:user.phonenumber,
                              IdNumber:user.idNumber,
                              email: user.email,
                              isActive:user.isActive,
                             
                              
    });
  }
  submitForm(){
    console.log(this.myForm.value);
    if(!this.myForm.valid){
      this.spinner.hide();
     return this.toastr.warning('Please fill in correct characters!')
    }
    this.spinner.show();
    this.userService.saveUser(this.myForm.value).subscribe(x =>{
      console.log(x);
      this.load();
      this.spinner.hide();
      this.toastr.success('Saved User Details!')
      this.modlService.dismissAll();
    })
  }
  load(){
    this.spinner.show();
    this.userService.getAll().subscribe(data =>{
        this.products = data;
        this.products.forEach((item,index) =>{
          if(item.email === 'admin@remedies.com'){
             this.products.splice(index, 1);
         }        
          
       })
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }
 
  onDelete(email){
    this.spinner.show();

    this.userService.deleteUser(email).subscribe(x => {
      
      this.toastr.success('Your account has been deleted!');
      this.load();
      this.spinner.hide();
      this.modlService.dismissAll();

     
    },error =>{
      this.toastr.error('Failed while deleting');
    })
  }

}
