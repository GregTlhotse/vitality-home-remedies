import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private apiUrl = environment.apiUrl;

  constructor(private httpClient:HttpClient) { }

  getCurrentAllProducts(){
    return this.httpClient.get<any>(this.apiUrl + 'Product/AllProducts');
  }
  saveProduct(data){
    return this.httpClient.post<string>(this.apiUrl + 'Product',data);

  }
  deleteProduct(id){
    return this.httpClient.delete(this.apiUrl + 'Product/Delete/' + id);

  }
  getProduct(id) {
    return this.httpClient.get(this.apiUrl + 'Product' + '?id=' + id);
  }
  //Prod Language
  getCurrentAllProductLanguages(){
    return this.httpClient.get<any>(this.apiUrl + 'ProductLanguage/AllProductLanguages');
  }
  getSearched(term){
    return this.httpClient.get<any>(this.apiUrl + 'ProductLanguage/GetSearched/'+term);
  }
  saveProductLanguage(data){
    return this.httpClient.post<string>(this.apiUrl + 'ProductLanguage',data);

  }
  deleteProductLanguage(id){
    return this.httpClient.delete(this.apiUrl + 'ProductLanguage/Delete/' + id);

  }
  getProductLanguage(id) {
    return this.httpClient.get(this.apiUrl + 'ProductLanguage' + '?id=' + id);
  }

   //Prod Doctors
   getCurrentAllProductDoctors(){
    return this.httpClient.get<any>(this.apiUrl + 'ProductDoctor/AllProductDoctors');
  }
  saveProductDoctor(data){
    return this.httpClient.post<string>(this.apiUrl + 'ProductDoctor',data);

  }
  deleteProductDoctor(id){
    return this.httpClient.delete(this.apiUrl + 'ProductDoctor/Delete/' + id);

  }
  getProductDoctor(id) {
    return this.httpClient.get(this.apiUrl + 'ProductDoctor' + '?id=' + id);
  }
}
