import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  private apiUrl = environment.apiUrl;

  constructor(private httpClient:HttpClient) { }
  saveDoctor(data){
    return this.httpClient.post<string>(this.apiUrl + 'Doctor',data);

  }
  getCurrentAllDocs(){
    return this.httpClient.get<any>(this.apiUrl + 'Doctor/AllUsers');
  }
  deleteUser(email){
    return this.httpClient.delete(this.apiUrl + 'Doctor/Delete/' + email);

  }
  getCurrentUser(id) {
    return this.httpClient.get(this.apiUrl + 'Doctor' + '?id=' + id);
  }
  requestDoctor(data){
    return this.httpClient.post<string>(this.apiUrl + 'Doctor/Request',data);

  }
}
