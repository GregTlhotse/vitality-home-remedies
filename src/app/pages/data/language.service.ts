import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  private apiUrl = environment.apiUrl;

  constructor(private httpClient:HttpClient) { }

  getCurrentAllLanges(){
    return this.httpClient.get<any>(this.apiUrl + 'Language/AllLanguages');
  }
  saveLanguageProd(data){
    return this.httpClient.post<string>(this.apiUrl + 'Language',data);

  }
  deleteLanguage(id){
    return this.httpClient.delete(this.apiUrl + 'Language/Delete/' + id);

  }
  getLanguage(id) {
    return this.httpClient.get(this.apiUrl + 'Language' + '?id=' + id);
  }
}
