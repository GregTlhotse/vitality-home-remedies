import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';
import { AddLanguageComponent } from './add-language/add-language.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AddUsersComponent } from './add-users/add-users.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ERROR_MESSAGES, NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { CUSTOM_ERRORS } from '../errors/custom-errors';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProductLanguageComponent } from './product-language/product-language.component';
import { ProductDoctorComponent } from './product-doctor/product-doctor.component';
import { DoctorReportComponent } from './reports/doctor-report/doctor-report.component';
import { ProductReportComponent } from './reports/product-report/product-report.component';
import { LanguageReportComponent } from './reports/language-report/language-report.component';
import { UserReportComponent } from './reports/user-report/user-report.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgBootstrapFormValidationModule.forRoot(),
    FontAwesomeModule

  ],
  declarations: [
    PagesComponent,
    AddDoctorComponent,
    AddLanguageComponent,
    AddProductComponent,
    AddUsersComponent,
    ProductLanguageComponent,
    ProductDoctorComponent,
    DoctorReportComponent,
    ProductReportComponent,
    LanguageReportComponent,
    UserReportComponent,
  ],
  providers: [
    {
      provide: CUSTOM_ERROR_MESSAGES,
      useValue: CUSTOM_ERRORS,
      multi: true
    }
  ]
})
export class PagesModule {
}
