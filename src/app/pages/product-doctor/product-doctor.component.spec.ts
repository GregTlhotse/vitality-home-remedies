import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDoctorComponent } from './product-doctor.component';

describe('ProductDoctorComponent', () => {
  let component: ProductDoctorComponent;
  let fixture: ComponentFixture<ProductDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
