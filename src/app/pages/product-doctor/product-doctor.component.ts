import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DoctorService } from '../data/doctor.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService } from '../data/language.service';
import { ProductService } from '../data/product.service';


@Component({
  selector: 'ngx-product-doctor',
  templateUrl: './product-doctor.component.html',
  styleUrls: ['./product-doctor.component.scss'],
  providers:[NgbActiveModal]

})
export class ProductDoctorComponent implements OnInit {
  closeResult:any;
  myForm: FormGroup;
products : any[];
languages : any[];
productLanguages: any[];
productDocs: any[];

docs : any[];

isEdit = false;
lang = null;
prod = null;

constructor(private modlService: NgbModal,
  private formBuilder: FormBuilder,
  private toastr: ToastrService,
  private doctorServc:DoctorService,
  private spinner:NgxSpinnerService,
  public activeModal: NgbActiveModal,
  private productService:ProductService,private languageServc:LanguageService) { }

  ngOnInit() {
    this.createForm();
    this.load();
    this.loadLang();
    this.loadProd();
    this.loadDocs();
  }
  open(content1) {
    this.createForm();
    this.lang = null;
    this.prod = null;
    this.isEdit = false;
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title',size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openEdit(id,content1) {
    this.getUser(id);
    this.modlService.open(content1, {ariaLabelledBy: 'modal-basic-title',size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      id: [null, []],
      product_Id: [null, [Validators.required]],
      doctor_Id: [null, [Validators.required]],


    });
  }//Product_Id Language_Id
  getUser(id){
    this.spinner.show();
    this.productService.getProductDoctor(id).subscribe(x =>{
      this.loadDataFromDatabase(x);
      this.spinner.hide();
      this.isEdit = true;

    })
  }
  loadDataFromDatabase(provider) {
    console.log(provider);
    this.myForm.patchValue({
                              id: provider.id,
                              product_Id: provider.product_Id,
                              doctor_Id: provider.doctor_Id,

                              
  
    });
  }
  submitForm(){
    console.log(this.myForm.value);
    if(!this.myForm.valid){
      this.spinner.hide();
     return this.toastr.warning('Please fill in correct characters!')
    }
    this.spinner.show();
    this.productService.saveProductDoctor(this.myForm.value).subscribe(x =>{
      console.log(x);
      this.load();
      this.spinner.hide();
      this.toastr.success('Saved Product Doctor Details!')
      this.modlService.dismissAll();
    })
  }
  load(){
    this.spinner.show();
    this.productService.getCurrentAllProductDoctors().subscribe(data =>{
        this.productDocs = data;
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }
  loadLang(){
    this.languageServc.getCurrentAllLanges().subscribe(data =>{
        this.languages = data;
        console.log(data)
    }, error =>{
      console.log(error)


    })
  }
  loadProd(){
    this.productService.getCurrentAllProducts().subscribe(data =>{
        this.products = data;
        console.log(data)
    }, error =>{
      console.log(error)


    })
  }
  loadDocs(){
    this.spinner.show();
    this.doctorServc.getCurrentAllDocs().subscribe(data =>{
        this.docs = data;
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }
  onDelete(id){
    this.spinner.show();
    this.productService.deleteProductDoctor(id).subscribe(x => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        this.modlService.dismissAll();
        this.load();
    }, 5000);
    this.toastr.success('Product Doctor  deleted!');

    },error =>{
      this.toastr.error('Failed while deleting');
    })
  }
  LanguageChange(data){
    this.spinner.show();
   let info =  this.languages.find(x => x.id === this.myForm.value.language_Id)
    this.lang = 'using '+ info.name;
    this.spinner.hide();

  }
  ProductChange(){
    this.spinner.show();
    let info =  this.products.find(x => x.id === this.myForm.value.product_Id)
     this.prod = info.name;
     this.spinner.hide();
  }


}
