import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService } from '../../data/language.service';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'ngx-language-report',
  templateUrl: './language-report.component.html',
  styleUrls: ['./language-report.component.scss']
})
export class LanguageReportComponent implements OnInit {
  languages : any[];
  allLanguages: any[];
  noResults
  searchTerm
  constructor( private spinner:NgxSpinnerService,
    private languageServc:LanguageService) { }

  ngOnInit() {
    this.load();
  }
  load(){
    this.spinner.show();
    this.languageServc.getCurrentAllLanges().subscribe(data =>{
        this.languages = data;
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }
  
  searchLanguage() {
    // this.searchTerm = ev.target.value;
    console.log(this.searchTerm);
    this.languageServc.getCurrentAllLanges().subscribe( (data) => {
     this.languages = data;
     this.allLanguages = data;
     this.noResults = false;
  
     if(!this.searchTerm){
       return this.languages = this.allLanguages;
     }
     this.languages = this.allLanguages.filter((item) => {
              return item.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
         });
         if(this.languages.length === 0){
           console.log("Success");
          this.noResults = true;
  
         }
  
   })
  }
  
  // orderByDate() {
  //   this.userSvc.getOrderByDate().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  // orderByDuration() {
  //   this.userSvc.getOrderByDuration().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  captureScreen()  
  {  
    this.spinner.show();
    var data = document.getElementById('contentToConvert');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('LanguagesReport.pdf'); // Generated PDF   
      this.spinner.hide();

    });  
  }
  
}
