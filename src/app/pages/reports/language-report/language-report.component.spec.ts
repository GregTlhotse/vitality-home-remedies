import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageReportComponent } from './language-report.component';

describe('LanguageReportComponent', () => {
  let component: LanguageReportComponent;
  let fixture: ComponentFixture<LanguageReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguageReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
