import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductService } from '../../data/product.service';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'ngx-product-report',
  templateUrl: './product-report.component.html',
  styleUrls: ['./product-report.component.scss']
})
export class ProductReportComponent implements OnInit {
  products : any[];
  allProducts: any[];
  noResults
  searchTerm
  constructor(private spinner:NgxSpinnerService,
    private productService:ProductService) { }

  ngOnInit() {
    this.load();
  }
  load(){
    this.spinner.show();
    this.productService.getCurrentAllProducts().subscribe(data =>{
        this.products = data;
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }
  
  searchProduct() {
    // this.searchTerm = ev.target.value;
    console.log(this.searchTerm);
    this.productService.getCurrentAllProducts().subscribe( (data) => {
     this.products = data;
     this.allProducts = data;
     this.noResults = false;
  
     if(!this.searchTerm){
       return this.products = this.allProducts;
     }
     this.products = this.allProducts.filter((item) => {
              return item.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
         });
         if(this.products.length === 0){
           console.log("Success");
          this.noResults = true;
  
         }
  
   })
  }
  
  // orderByDate() {
  //   this.userSvc.getOrderByDate().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  // orderByDuration() {
  //   this.userSvc.getOrderByDuration().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  captureScreen()  
  {  
    this.spinner.show();
    var data = document.getElementById('contentToConvert');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('ProductsReport.pdf'); // Generated PDF   
      this.spinner.hide();

    });  
  }
  
}
