import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../../data/user.service';
import { ProductService } from '../../data/product.service';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'ngx-user-report',
  templateUrl: './user-report.component.html',
  styleUrls: ['./user-report.component.scss']
})
export class UserReportComponent implements OnInit {
users
allUsers: any[];
noResults
searchTerm
  constructor(private spinner:NgxSpinnerService,
    private productService:ProductService,private userService:UserService) { }

  ngOnInit() {
    this.load();
  }
  load(){
    this.spinner.show();
    this.userService.getAll().subscribe(data =>{
        this.users = data;
        this.users.forEach((item,index) =>{
          if(item.email === 'admin@remedies.com'){
             this.users.splice(index, 1);
         }        
          
       })
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }

  searchUser() {
    // this.searchTerm = ev.target.value;
    console.log(this.searchTerm);
    this.userService.getAll().subscribe( (data) => {
      data.forEach((item,index) =>{
        if(item.email === 'admin@remedies.com'){
          data.splice(index, 1);
       }        
        
     })
     this.users = data;
    
     this.allUsers = data;
     this.noResults = false;
  
     if(!this.searchTerm){
       return this.users = this.allUsers;
     }
     this.users = this.allUsers.filter((item) => {
              return item.firstName.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
         });
         if(this.users.length === 0){
           console.log("Success");
          this.noResults = true;
  
         }
  
   })
  }
  
  // orderByDate() {
  //   this.userSvc.getOrderByDate().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  // orderByDuration() {
  //   this.userSvc.getOrderByDuration().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  captureScreen()  
  {  
    this.spinner.show();
    var data = document.getElementById('contentToConvert');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('UsersReport.pdf'); // Generated PDF   
      this.spinner.hide();

    });  
  }
  
}
