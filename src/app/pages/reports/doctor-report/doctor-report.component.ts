import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { DoctorService } from '../../data/doctor.service';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'ngx-doctor-report',
  templateUrl: './doctor-report.component.html',
  styleUrls: ['./doctor-report.component.scss']
})
export class DoctorReportComponent implements OnInit {
  docs : any[];
allDocs: any[];
noResults
searchTerm
  constructor(private doctorServc:DoctorService,private spinner:NgxSpinnerService) { }

  ngOnInit() {
    this.load();
  }
  load(){
    this.spinner.show();
    this.doctorServc.getCurrentAllDocs().subscribe(data =>{
        this.docs = data;
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)

      this.spinner.hide();

    })
  }

  searchDoctor() {
    // this.searchTerm = ev.target.value;
    console.log(this.searchTerm);
    this.doctorServc.getCurrentAllDocs().subscribe( (data) => {
     this.docs = data;
     this.allDocs = data;
     this.noResults = false;
  
     if(!this.searchTerm){
       return this.docs = this.allDocs;
     }
     this.docs = this.allDocs.filter((item) => {
              return item.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
         });
         if(this.docs.length === 0){
           console.log("Success");
          this.noResults = true;
  
         }
  
   })
  }
  
  // orderByDate() {
  //   this.userSvc.getOrderByDate().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  // orderByDuration() {
  //   this.userSvc.getOrderByDuration().subscribe(data => {
  //     console.log(data);
  //     this.calls = data;
  //   })
  // }
  captureScreen()  
  {  
    this.spinner.show();
    var data = document.getElementById('contentToConvert');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('DoctorsReport.pdf'); // Generated PDF   
      this.spinner.hide();

    });  
  }
  


}
