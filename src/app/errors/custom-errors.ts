import {ErrorMessage} from 'ng-bootstrap-form-validation';

export const CUSTOM_ERRORS: ErrorMessage[] = [
  {
    error: 'required',
    format: requiredFormat
  }, {
    error: 'email',
    format: emailFormat
  }
];

export function requiredFormat(label: string): string {
  return `${label} is required`;
}

export function emailFormat(label: string): string {
  return `${label} doesn't look like a valid email address.`;
}
