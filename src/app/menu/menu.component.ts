import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { RegisterComponent } from '../register/register.component';
import { LoginComponent } from '../login/login.component';
import { ProfileComponent } from '../profile/profile.component';




@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  navbarOpen = false;
   isLogin: boolean = false;
  name: string;
  constructor(private modalService: NgbModal,private router: Router,
    ) { }

  ngOnInit() {
    if(sessionStorage.getItem('token') !== null){
      this.isLogin = true;
      this.name = localStorage.getItem('name');
    }
}
openRegisterFormModal(){
  const modalRef = this.modalService.open(RegisterComponent,
    {size: 'lg',
    centered: true });
  modalRef.componentInstance.id = 1;
  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });

}
openFormModal(){
  const modalRef = this.modalService.open(LoginComponent,
    {size: 'lg',
    centered: true });
  modalRef.componentInstance.id = 10;
  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });
  // this.toggleNavbar();
}
toggleNavbar() {
  this.navbarOpen = !this.navbarOpen;
}

signOut(): void {
  localStorage.removeItem('name');
  sessionStorage.removeItem('token');
  localStorage.removeItem('ID');
  this.router.navigate(['/home']);
  setTimeout(() => {
    location.reload();
  }, 1000);

}
openFormModalProfile(){
  const modalRef = this.modalService.open(ProfileComponent,
    {size: 'lg',
    centered: true });
  modalRef.componentInstance.id = 10;
  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });
  this.toggleNavbar();
}
}
