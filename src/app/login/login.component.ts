import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from '../register/register.component';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { UserService } from '../data/user.service';
import { GenerateCodeComponent } from '../generate-code/generate-code.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input() id: number;
  myForm: FormGroup;
loggedIn: boolean;

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder,
    private modalService: NgbModal,private toastr: ToastrService
    ,private spinner: NgxSpinnerService,private router:Router,private userService:UserService) { }

  ngOnInit() {
    this.createForm();
  }
 
  
  
  closeModal() {
    this.modalService.dismissAll();
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      email: ['', [Validators.required,Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      password: ['', [Validators.required, ]],

    });
  }
  openFormModal() {
    this.closeModal();
    const modalRef = this.modalService.open(RegisterComponent,
      {size: 'lg',
      centered: true });
    modalRef.componentInstance.id = 1;
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    // this.toggleNavbar();
  }
  submitForm() {
    this.spinner.show();

    if (!this.myForm.valid) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);

      return this.toastr.error('Please Fill In The Fields...', 'Error');
    }
    this.userService.login(this.myForm.value).subscribe(res => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        location.reload();
    }, 5000);

      this.toastr.success('Congratulations! You logged on successfully.');
      sessionStorage.setItem('token', res['token']);
      localStorage.setItem('name', res['user'].firstName);
      localStorage.setItem('ID', res['user'].id);
      if(res['user'].email === 'admin@remedies.com'){
        this.myForm.reset();
         this.closeModal();
        return  this.router.navigateByUrl('/pages')
      }
      if(sessionStorage.getItem('outside') !== null){
        this.myForm.reset();
         this.closeModal();
        return  this.router.navigateByUrl('/doctor');

      }
      this.router.navigateByUrl('/home')

      console.log(res['user'].firstName);
      this.myForm.reset();
      this.closeModal();
    }, error => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
    console.log(error);
    
    if(error.error === 'User not found'){
      this.toastr.error('Oops! User not found', 'Login Error');
    }
    if(error.error === 'Incorrect Password'){
      this.toastr.error('Oops! Incorrect Password', 'Login Error');
    }
    //
    if(error.error === 'Username and Password Not found, Please sign-up first'){
      this.toastr.error('Oops! Username and Password Not found, Please register first', 'Login Error');
    }
    });

  }
  openFormModalGenCode() {
    this.closeModal();
    const modalRef = this.modalService.open(GenerateCodeComponent,
      {size: 'lg',
      centered: true });
    modalRef.componentInstance.id = 2;
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    // this.toggleNavbar();
  }
}
