import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../data/user.service';

@Component({
  selector: 'ngx-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers:[NgbActiveModal]
})
export class ResetPasswordComponent implements OnInit {
  @Input() id: number;
  myForm: FormGroup;
  email: any = null;

  constructor(private router: Router,private activeRoute: ActivatedRoute,public activeModal: NgbActiveModal, private formBuilder: FormBuilder,
    private modalService: NgbModal,private toastr: ToastrService
    ,private svc: UserService,private spinner: NgxSpinnerService) {   
       this.createForm();
    }

  ngOnInit() {
    
this.email = sessionStorage.getItem('resetEmail');
      
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      email: ['', []],
      newPassword: ['', [Validators.required]],
      renewPassword: ['', [Validators.required]],
      passwordResetCode: ['', [Validators.required]],

    });
  }
  closeModal() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('/home');
  }
  submitForm() {
    this.spinner.show();

    if (this.myForm.value.newPassword !== this.myForm.value.renewPassword) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      return this.toastr.error('Passwords Are Not The Same!', 'Error');
    }

    if (!this.myForm.valid) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);

      return this.toastr.error('Please Fill In The Fields...', 'Error');
    }
    this.myForm.value.email = this.email;
    console.log(this.myForm.value);
    this.svc.resetPassword(this.myForm.value).subscribe(res => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);

      this.toastr.success('Congratulations! You have successfuly updated your password.');

      this.myForm.reset();
      this.closeModal();
      this.router.navigateByUrl('/home')
    }, error => {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      this.toastr.error('Oops! Something went wrong on the server', 'Registration Error');
      console.log(error);
    });

  }
  openFormModal() {
    const modalRef = this.modalService.open(ResetPasswordComponent,
      {size: 'lg',
      centered: true });
    modalRef.componentInstance.id = 2;
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    // this.toggleNavbar();
  }

}
