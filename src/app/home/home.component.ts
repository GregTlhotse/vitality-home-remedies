import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoginComponent } from '../login/login.component';
import { ProductService } from '../pages/data/product.service';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  p: any;
  noResults = false;
  searchTerm:any;
  products : any[];
count = 0;
viewData = null;
viewDetails = false;
email: any = null;

  constructor(private productService:ProductService,
     private spinner:NgxSpinnerService,private modalService: NgbModal,private router: Router,private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.email = params['email'];
      sessionStorage.setItem('resetEmail',this.email);
      if(this.router.url.indexOf('reset') > 0){
        this.openResetPasswordComponent();
      }
    });
  }
  searchPrograms(){
    this.spinner.show();
    this.productService.getSearched(this.searchTerm).subscribe(data =>{
        this.products = data;
        if(this.products !== null){
          this.count = this.products.length;
          if(this.products.length > 3){
            this.noResults = true;
          }
        }
        
        console.log(data)
        this.spinner.hide();
    }, error =>{
      console.log(error)
      this.spinner.hide();

    })
  }
  load(){
    
  }
  getDetails(info){
    console.log(info);
    this.viewData = info;
    sessionStorage.setItem('doctorEmail', info.doctor.email);
    this.viewDetails = true;
  }
  back(){
    this.viewDetails = false;

  }
  openFormModal(){
    const modalRef = this.modalService.open(LoginComponent,
      {size: 'lg',
      centered: true });
    modalRef.componentInstance.id = 10;
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    // this.toggleNavbar();
  }
  book(){
    if(sessionStorage.getItem('token') === null){
      let num = 1;
      sessionStorage.setItem('outside',num.toString());
       this.openFormModal();
    }
    if(sessionStorage.getItem('token') !== null){
      this.router.navigateByUrl('/doctor');
   }
  }
  openResetPasswordComponent() {
    const modalRef = this.modalService.open(ResetPasswordComponent,
      {size: 'lg',
      centered: true });
    modalRef.componentInstance.id = 2;
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    // this.toggleNavbar();
  }
}
