import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
     <a  target="_blank"><b>Vitality Home Remedies Team</b></a> 2020
    </span>
    <div class="socials">
      <a  target="_blank" class="ion ion-social-facebook"></a>
      <a  target="_blank" class="ion ion-social-twitter"></a>
    </div>
  `,
})
export class FooterComponent {
}
