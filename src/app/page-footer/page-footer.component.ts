import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-footer',
  templateUrl: './page-footer.component.html',
  styleUrls: ['./page-footer.component.scss']
})
export class PageFooterComponent implements OnInit {
  year: number;
  isTrue: boolean = false;
  constructor() {
    if(sessionStorage.getItem('token') !== null){
      this.isTrue = true;
    }
   }

  ngOnInit() {
    this.getYear();

  }
  getYear() {
    this.year = new Date().getFullYear();
  return this.year;
  }
  click(info: string)
{
  if(info === 'facebook'){
    return window.open('https://www.facebook.com/', "_blank");
  }
  if(info === 'twitter'){
    return window.open('https://twitter.com/', "_blank");

  }
  if(info === 'instagram'){
    return window.open('https://www.instagram.com/', "_blank");

  }
  if(info === 'linkedin'){
    return  window.open('https://www.linkedin.com/', "_blank");

  }
}

}
