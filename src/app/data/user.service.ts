import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl = environment.apiUrl;

  constructor(private httpClient:HttpClient) { }

  saveUser(data){
    return this.httpClient.post<string>(this.apiUrl + 'User',data);

  }
  login(data) {
    return this.httpClient.post<string>(this.apiUrl + 'User/login', data);
  }
  getCurrentUser(userid: string): Observable<any> {
    return this.httpClient.get<any>(this.apiUrl + 'User' + '?id=' + userid);
  }
  deleteUser(email){
    return this.httpClient.delete(this.apiUrl + 'User/Delete/' + email);

  }
  getAll(){
    return this.httpClient.get<any>(this.apiUrl + 'User/AllUsers');

  }
  generatePasswordCode(data) {
    return this.httpClient.get(this.apiUrl + "User/GeneratePasswordCode/" + data);
  }
 
  resetPassword(data) {
    return this.httpClient.post(this.apiUrl + "User/ResetPassword", data);
  }
}
